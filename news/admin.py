from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

# Register your models here.
from .models import *

class TblCategotyAdmin(admin.ModelAdmin):
	model = TblCategoty
	list_display = ['category_name']

class TblContentTypeAdmin(admin.ModelAdmin):
	model = TblContentType
	list_display = ['content_type']

class TblPostAdmin(admin.ModelAdmin):
	model = TblPost
	list_display = ['title', 'category']

admin.site.register(TblCategoty, TblCategotyAdmin)
admin.site.register(TblContentType, TblContentTypeAdmin)
admin.site.register(TblPost, TblPostAdmin)
admin.site.register(TblContent)

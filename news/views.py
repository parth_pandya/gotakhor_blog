from django.shortcuts import render
from .models import *

# Create your views here.
def index(request):
	titles = TblPost.objects.all().order_by('date_time').reverse()[:6]
	popular_posts = TblPost.objects.filter(is_popular=True).order_by('date_time').reverse()[:4]
	videos = TblPost.objects.all().order_by('date_time').reverse()[:3]
	memes = TblPost.objects.all().order_by('date_time').reverse()[:3]
	return render(request, 'home-02.html', {'posts': titles, 'popular_posts':popular_posts,
											'videos':videos, 'memes':memes})


def post_detail(request, pk):
	print("pk =============   ", pk)
	title = TblPost.objects.get(pk=pk)
	print("title = = = = ", title)
	article = TblContent.objects.get(post = pk).order_by('date_time').reverse()[:5]
	print("article - == =  ", article)
	popular_post = TblPost.objects.filter(is_popular=True).order_by('date_time').reverse()[:5]
	return render(request, 'blog-detail-01.html', {'article':article,'title':title, 'popular_posts':popular_post})


def menu_tab(request, category):
	print("=====================   in news tab ==========================", category)
	try:
		title = TblPost.objects.filter(category=TblCategoty.objects.get(category_name=category.lower()).id).order_by('date_time').reverse()
		print("title = = = ", title)
	except Exception as e:
		print("exception occured in menu_tab", e)
		title = {}
	return render(request, 'blog-list-01.html', {'title':title, 'category_name':category.title()})

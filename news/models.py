# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = True` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from ckeditor.fields import RichTextField

class TblCategoty(models.Model):
    category_name = models.CharField(max_length=200)
    is_deleted = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'tbl_categoty'

    def __str__(self):
       return self.category_name

class TblContentType(models.Model):
    content_type = models.CharField(max_length=200)
    is_deleted = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'tbl_content_type'

    def __str__(self):
       return self.content_type

class TblPost(models.Model):
    category = models.ForeignKey(TblCategoty, models.DO_NOTHING)
    title = models.CharField(max_length=500)
    is_published = models.BooleanField()
    post_image = models.CharField(max_length=500)
    date_time = models.DateTimeField()
    is_popular = models.BooleanField(default=False)
    popular_image = models.CharField(max_length=500)
    author_name = models.CharField(max_length=500)

    class Meta:
        managed = True
        db_table = 'tbl_post'

    def __str__(self):
       return self.title

class TblContent(models.Model):
    post = models.ForeignKey(TblPost, models.DO_NOTHING)
    content = RichTextField()
    is_deleted = models.BooleanField()

    class Meta:
        managed = True
        db_table = 'tbl_content'
